package com.sj1688.register.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LoginController {
    @RequestMapping(value = "/login")
//    @ResponseStatus(org.springframework.http.HttpStatus.UNAUTHORIZED)
    public String showLoginForm(HttpServletRequest req, Model model) {
        String exceptionClassName = (String)req.getAttribute("shiroLoginFailure");
        String error = null;
//       String name = (String)req.getParameter("username");
//       if(name!= null){
//    		return "redirect:/";
//       }
        
        if(UnknownAccountException.class.getName().equals(exceptionClassName)) {
            error = "用户名/密码错误";
        } else if(IncorrectCredentialsException.class.getName().equals(exceptionClassName)) {
            error = "用户名/密码错误";
        } else if(exceptionClassName != null) {
        	System.out.println(exceptionClassName);
          error = "其他错误：" + exceptionClassName;
        }
        model.addAttribute("error", error);
        
//        String name = (String)req.getParameter("username");
//        String password = (String)req.getParameter("password");
       String val = req.getParameter("rememberMe");
//        Subject subject = SecurityUtils.getSubject();  
//        UsernamePasswordToken token = new UsernamePasswordToken(name, password);  
//        token.setRememberMe(true);  
//        subject.login(token);  
        System.out.println(111);
        return "login";
    }
    @RequestMapping(value = "logout", method = RequestMethod.GET)
	public String logout() {
		SecurityUtils.getSubject().logout();
		return "redirect:/";
	}
  
}
