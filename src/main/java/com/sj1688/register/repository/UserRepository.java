package com.sj1688.register.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.sj1688.register.entity.User;


public interface UserRepository extends JpaRepository<User, Long>{
	User findByUsername(String username);
	
}