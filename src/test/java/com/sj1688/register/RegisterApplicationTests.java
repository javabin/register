package com.sj1688.register;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.sj1688.register.RegisterApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = RegisterApplication.class)
@WebAppConfiguration
public class RegisterApplicationTests {

	@Test
	public void contextLoads() {
	}

}
